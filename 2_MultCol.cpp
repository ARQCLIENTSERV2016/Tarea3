#include <algorithm>
#include <chrono>
#include <condition_variable>
#include <iostream>
#include <random>
#include <thread>
#include <vector>

#include "Util/Matrix.hpp"
#include "Util/SafeQueue.hpp"

#define TAM 2000

using namespace std;
using namespace std::chrono;

static unsigned int gMaxThreads = thread::hardware_concurrency() - 1;

Matrix<float> Multyply(const Matrix<float>& m1, const Matrix<float>& m2) {
    // Vector que contiene todos los hilos disponibles para ejecutar la
    // multiplicación
    vector<thread> tvector(gMaxThreads);

    // Variable condiciona
    condition_variable cv;
    mutex mtx;

    // Esta cola se usa para saber que hilo ya ha terminado su ejecución
    SafeQueue<size_t> q;
    for (size_t i = 0; i < tvector.size(); i++) {
        q.push(i);
    }

    // La matriz resultante
    Matrix<float> result(m1.NumRows(), m2.NumCols());

    // Función que ejecuta cada hilo en el cual se le asigna el indice del hilo
    // y la columna la que está multiplicando
    auto mult_col = [&](size_t index, size_t col) -> void {
        for (size_t row = 0; row < m1.NumRows(); row++) {
            float dot = 0.f;
            for (size_t num = 0; num < m1.NumCols(); num++) {
                dot += m1(row, num) * m2(num, col);
            }
            result(row, col) = dot;
        }

        // Agrega el indice a la cola de terminados
        q.push(index);

        // Notifica el proceso principal que ya se ha terminado la ejecución
        cv.notify_one();
    };

    // Por cada columna de la matrix 2 se lanza un hilo
    for (size_t col = 0; col < m2.NumCols(); col++) {
        // Esperar hasta que un hilo finalize
        if (q.empty()) {
            unique_lock<mutex> lk(mtx);
            cv.wait(lk, [&q] { return !q.empty(); });
        }

        // Extrae el indice del ultimo hilo disponible
        size_t index = q.front();
        q.pop();

        // Espera a que termine en caso de que la función que esté ejecutando
        // el hilo aún no halla terminado
        if (tvector[index].joinable()) {
            tvector[index].join();
        }

        // Ejecuta un nuevo hilo con el indice y columna indicados
        tvector[index] = thread(mult_col, index, col);
    }

    // Espera a que todos los hilos terminen
    for (auto& t : tvector) {
        t.join();
    }

    return result;
}

Matrix<float> RandomMatrix(size_t rows, size_t cols, int min = 0,
                           int max = 10) {
    random_device rd;
    mt19937 gen(rd());
    uniform_int_distribution<int> dis(min, max);

    Matrix<float> m(rows, cols);

    generate(m.GetData().begin(), m.GetData().end(),
             [&] { return static_cast<float>(dis(gen)); });

    return m;
}

int main() {
    cout << "Usando " << gMaxThreads << " hilos." << endl;
    cout << "Multiplicando matrices de tamaño [" << TAM << ", " << TAM
         << "] * [" << TAM << ", " << TAM << "]\n";

    Matrix<float> m1 = RandomMatrix(TAM, TAM);
    Matrix<float> m2 = RandomMatrix(TAM, TAM);

    Matrix<float> r1, r2;

    {
        auto tstart = high_resolution_clock::now();
        r1 = Multyply(m1, m2);
        auto tend = high_resolution_clock::now();
        auto elapsed = duration_cast<nanoseconds>(tend - tstart).count();
        cout << "Hilos: " << elapsed / 1000000000.0 << " s" << endl;
    }

    {
        auto tstart = high_resolution_clock::now();
        r2 = (m1 * m2);
        auto tend = high_resolution_clock::now();
        auto elapsed = duration_cast<nanoseconds>(tend - tstart).count();
        cout << "Normal: " << elapsed / 1000000000.0 << " s" << endl;
    }

    cout << "Las matrices son iguales: " << boolalpha << (r1 == r2) << endl;
    return 0;
}
