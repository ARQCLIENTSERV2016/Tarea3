cmake_minimum_required(VERSION 2.8.12)

if(${CMAKE_SOURCE_DIR} STREQUAL ${CMAKE_BINARY_DIR})
    message(
        FATAL_ERROR
        "Do not build in-source. Please remove CMakeCache.txt and the "
        "CMakeFiles/ directory. Then build out-of-source."
    )
endif()

project(Tarea3)

if (NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE "Release")
endif()

# Adding C++11 support in GCC compiler and Clang compiler
if(CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
    if(CMAKE_SYSTEM_NAME STREQUAL "Linux")
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -stdlib=libstdc++")
    else()
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -stdlib=libc++")
    endif()
elseif(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
endif()

# If compiler is GNU GCC or Clang enable the warnings
if(CMAKE_CXX_COMPILER_ID STREQUAL "GNU" OR
   CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -pedantic")
endif()

# Find the system Thread library
find_package(Threads)

include_directories("${CMAKE_SOURCE_DIR}")

add_subdirectory(Tests)

add_executable(DumpDataset DumpDataset.cpp)
target_link_libraries(DumpDataset ${CMAKE_THREAD_LIBS_INIT})

add_executable(2_MultCol 2_MultCol.cpp)
target_link_libraries(2_MultCol ${CMAKE_THREAD_LIBS_INIT})

# add_executable(BlockDiamond BlockDiamond.cpp)
# target_link_libraries(BlockDiamond ${CMAKE_THREAD_LIBS_INIT})
