#include <stdlib.h>
#include <time.h>
#include <chrono>
#include <iostream>
#include <random>
#include <thread>
#include <vector>
#include "Util/Matrix.hpp"
#include "Util/SafeQueue.hpp"
#define TAM 20
using namespace std;
using namespace chrono;

void MutixMatrzT(Matrix<float>& m1, Matrix<float>& m2, Matrix<float>& r1,
                 int row, int column, SafeQueue<int>& q, int ind) {
    for (size_t i = 0; i < TAM; i++) {
        r1(row, column) += m1(row, i) * m2(i, column);
    }
    // agregar cola segura. id...
    q.push(ind);
}

void Multyply(Matrix<float>& m1, Matrix<float>& m2, Matrix<float>& r1) {
    vector<thread> t(thread::hardware_concurrency());
    SafeQueue<int> q;
    for (int i = 0; i < t.size(); i++) {
        q.push(i);
    }
    // crear cola segura...
    for (int i = 0; i < TAM; i++) {
        for (int j = 0; j < TAM; j++) {
            while (q.empty()) {
                // this_thread::sleep_for(milliseconds(10));
            }
            int ind = q.front();
            if (t[ind].joinable()) t[ind].join();
            t[ind] = thread(MutixMatrzT, ref(m1), ref(m2), ref(r1), i, j,
                            ref(q), ind);
            q.pop();
        }
    }
    // MulMatTh M1(m1, m2, r1, i, i);
    // t.emplace_back(&MulMatTh::Multiply, &M1);
    for (auto& t1 : t) {
        t1.join();  // sincronizacion del hilo padre con el hilo hijo.
    }
}

int main() {
    // vector < vector<float> prueba(TAM, vector<float>(TAM));
    int n = thread::hardware_concurrency();
    cout << "Usando " << n << " hilos." << endl;

    Matrix<float> m1(TAM, TAM);
    Matrix<float> m2(TAM, TAM);
    Matrix<float> r1(TAM, TAM);

    random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<int> dis(0, 10);

    for (int i = 0; i < TAM; i++) {
        for (int j = 0; j < TAM; j++) {
            m1(i, j) = dis(gen);
            m2(i, j) = dis(gen);
        }
    }

    Multyply(m1, m2, r1);

    cout << "Resultado:" << endl;
    cout << r1 << endl;
    return 0;
}
