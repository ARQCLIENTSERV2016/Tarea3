#include <algorithm>
#include <chrono>
#include <fstream>
#include <iostream>
#include <sstream>
#include <tuple>

#include "Util/SparseMatrix.hpp"
#include "Util/MatrixFileUtils.hpp"

int main(int argc, char* argv[]) {
    if (argc != 3) {
        std::cout << "Usage: " << argv[0] << " dataset output" << std::endl;
        return 1;
    }

    if (!FileExists(argv[1])) {
        std::cout << "File " << argv[1] << " does not exists." << std::endl;
        return 2;
    }

    SparseMatrix<float> mat = LoadDatasetFromFile<float>(argv[1]);

    CreateFile(argv[2]);
    DumpMatrixToFile(mat, argv[2]);

    return 0;
}
