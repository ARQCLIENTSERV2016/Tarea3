#pragma once

#include <cassert>
#include <functional>
#include <istream>
#include <map>
#include <ostream>
#include <vector>

#include "SparseVector.hpp"
#include "ThreadPool.hpp"

template <typename T>
class SparseMatrix {
public:
    SparseMatrix() {}
    ~SparseMatrix() {}
    SparseMatrix(size_t rows, size_t cols) : rows_(rows, SparseVector<T>(cols)) {}

    // template <typename U>
    // SparseMatrix(const SparseMatrix<U>& m, size_t row_pos, size_t col_pos,
    //              size_t num_rows, size_t num_cols)
    //       : NumRows()(num_rows),
    //         NumCols()(num_cols),
    //         data_(num_rows * num_cols, T(0)) {
    //     if (row_pos >= m.NumRows()) size_t end_row = (row_pos + NumRows());
    //     size_t end_col = (col_pos + NumCols());

    //     if (end_row > m.NumRows() || end_row < row_pos) end_row =
    //     m.NumRows();
    //     if (end_col > m.NumCols() || end_col < col_pos) end_col =
    //     m.NumCols();

    //     for (size_t row = row_pos; row < end_row; row++) {
    //         for (size_t col = col_pos; col < end_col; col++) {
    //             T val = m.Get(row, col);
    //             Set(cont++] = ;
    //         }
    //     }
    // }

    SparseMatrix(const SparseMatrix<T>&) = default;

    SparseMatrix(SparseMatrix<T>&&) = default;

    SparseMatrix<T>& operator=(const SparseMatrix<T>& other) = default;

    SparseMatrix<T>& operator=(SparseMatrix<T>&& other) = default;

    size_t NumRows() const {
        return rows_.size();
    }

    size_t NumCols() const {
        return rows_[0].Size();
    }

    T Get(size_t r, size_t c) const {
        if (r >= NumRows() || c >= NumCols()) {
            throw std::out_of_range("the index is not in the range");
        }

        return rows_[r].Get(c);
    }

    void Set(const T& val, size_t r, size_t c) {
        if (r >= NumRows() || c >= NumCols()) {
            throw std::out_of_range("the index is not in the range");
        }

        rows_[r].Set(val, c);
    }

    void SetAdd(const T& val, size_t r, size_t c) {
        if (r >= NumRows() || c >= NumCols()) {
            throw std::out_of_range("the index is not in the range");
        }

        rows_[r].SetAdd(val, c);
    }
    void SetAddDiamond(const T& val, size_t r, size_t c, T& max_value) {
        if (r >= NumRows() || c >= NumCols()) {
            throw std::out_of_range("the index is not in the range");
        }

        rows_[r].SetAddDiamond(val, c, max_value);
    }

    void SetData(const std::vector<T>& data) {
        if (data.size() != NumRows() * NumCols()) {
            throw std::out_of_range("invalid data size.");
        }
        for (size_t row = 0; row < NumRows(); row++) {
            for (size_t col = 0; col < NumCols(); col++) {
                T val = data[row * NumCols() + col];
                if (val == T(0)) continue;
                Set(val, row, col);
            }
        }
    }

    SparseMatrix<T> Mult(const SparseMatrix<T>& b) const {
        // http://mathforum.org/library/drmath/view/51903.html
        assert(NumCols() == b.NumRows());

        // Create the result SparseMatrix
        SparseMatrix<T> result(NumRows(), b.NumCols());

        // Iterate over each SparseVector in the first matrix
        for (size_t row = 0; row < NumRows(); row++) {
            const SparseVector<T>& current_row = rows_[row];

            // Only iterate over the non-zero values of the SparseVector
            for (size_t i = 0; i < current_row.NumNonZero(); i++) {
                // The value and position at index i of current_row
                size_t col = current_row.indices_[i];
                T val_a = current_row.vals_[i];

                // Get the correspondent SparseVector that the value multiply with
                const SparseVector<T>& current_row_b = b.rows_[col];

                // Only iterate over the non-zero values of the SparseVector
                for (size_t x = 0; x < current_row_b.NumNonZero(); x++) {
                    // The value and position at index x of current_row
                    size_t col_b = current_row_b.indices_[x];
                    T val_b = current_row_b.vals_[x];

                    // Multiply the two values
                    T mult = val_a * val_b;

                    // Add the value to the result SparseMatrix
                    result.SetAdd(mult, row, col_b);
                }
            }
        }

        return result;
    }

    SparseMatrix<T> MultOld(const SparseMatrix<T>& b) const {
        // http://gauss.cs.ucsb.edu/~aydin/csb2009.pdf
        assert(NumCols() == b.NumRows());

        SparseMatrix<T> result(NumRows(), b.NumCols());

        for (size_t row = 0; row < NumRows(); row++) {
            const SparseVector<T>& current_row = rows_[row];
            for (size_t col = 0; col < b.NumCols(); col++) {
                T temp(0);
                for (size_t i = 0; i < current_row.indices_.size(); i++) {
                    temp += current_row.vals_[i] * b.Get(current_row.indices_[i], col);
                }
                if (temp == T(0)) continue;
                result.Set(temp, row, col);
            }
        }

        return result;
    }

    SparseMatrix<T> MultConcurrent(const SparseMatrix<T>& /*b*/) const {
        return SparseMatrix<T>();
    }

    SparseMatrix<T> MultConcurrentOld(const SparseMatrix<T>& b) const {
        // http://gauss.cs.ucsb.edu/~aydin/csb2009.pdf
        assert(NumCols() == b.NumRows());
        // Create a ThreadPool to execute concurrent tasks
        ThreadPool* pool = new ThreadPool();
        // A Matrix to store the result
        SparseMatrix<T> result(NumRows(), b.NumCols());
        // This lambda function is in charge of multiply by each row
        auto mult_row = [&](size_t row) {
            const SparseVector<T>& current_row = rows_[row];
            SparseVector<T>& result_row = result.rows_[row];
            // aca deberia recorrerse por la cantidad de datos que hay en la
            // fila
            for (size_t col = 0; col < b.NumCols(); col++) {
                T temp(0);
                for (size_t i = 0; i < current_row.indices_.size(); i++) {
                    temp += current_row.vals_[i] * b.Get(current_row.indices_[i], col);
                }
                if (temp == T(0)) continue;
                result_row.Set(temp, col);
            }
        };

        // Register each task in the ThreadPool
        for (size_t row = 0; row < NumRows(); row++) {
            pool->Submit([&mult_row, row] { mult_row(row); });
        }

        // Wait all the task to be completed and delete the ThreadPool
        delete pool;

        return result;
    }

    template <typename U>
    static SparseMatrix<U> Diamond(SparseMatrix<U>& a, SparseMatrix<U>& b, U& max_value) {
        SparseMatrix<U> result(a.NumRows(), b.NumRows());

        for (size_t row = 0; row < a.NumRows() - 1; row++) {
            const SparseVector<U>& current_row = a.rows_[row];
            for (size_t i = 0; i < current_row.NumNonZero(); i++) {
                // The value and position at index i of current_row
                size_t col = current_row.indices_[i];
                T val_a = current_row.vals_[i];

                // Get the correspondent SparseVector that the value multiply with
                const SparseVector<U>& current_row_b = b.rows_[col];

                // Only iterate over the non-zero values of the SparseVector
                for (size_t x = 0; x < current_row_b.NumNonZero(); x++) {
                    // The value and position at index x of current_row
                    size_t col_b = current_row_b.indices_[x];
                    T val_b = current_row_b.vals_[x];

                    // Add the two values
                    T sum = val_a + val_b;

                    // Add the value to the result SparseMatrix
                    result.SetAddDiamond(sum, row, col_b, max_value);
                }
            }
        }
        return result;
    }

    template <typename U>
    static SparseMatrix<U> DiamondConcurrent(SparseMatrix<U>& a, SparseMatrix<U>& b, U& max_value) {
        ThreadPool* pool = new ThreadPool();
        SparseMatrix<U> result(a.NumRows(), b.NumCols());

        auto diamond_row = [&](size_t row) {
            const SparseVector<U>& current_row = a.rows_[row];
            for (size_t i = 0; i < current_row.NumNonZero(); i++) {
                // The value and position at index i of current_row
                size_t col = current_row.indices_[i];
                T val_a = current_row.vals_[i];

                // Get the correspondent SparseVector that the value multiply with
                const SparseVector<U>& current_row_b = b.rows_[col];

                // Only iterate over the non-zero values of the SparseVector
                for (size_t x = 0; x < current_row_b.NumNonZero(); x++) {
                    // The value and position at index x of current_row
                    size_t col_b = current_row_b.indices_[x];
                    T val_b = current_row_b.vals_[x];

                    // Add the two values
                    T sum = val_a + val_b;

                    // Add the value to the result SparseMatrix
                    result.SetAddDiamond(sum, row, col_b, max_value);
                }
            }
        };

        for (size_t row = 0; row < a.NumRows(); row++) {
            std::cout << "Rows " << row << std::endl;
            pool->Submit([&diamond_row, row] { diamond_row(row); });
        }

        delete pool;

        return result;
    }

    template <typename U>
    static U DiamondRecursive(SparseMatrix<U>& a) {
        U max_value(std::numeric_limits<U>::min());

        std::function<SparseMatrix<U>(SparseMatrix<U>&, int)> Recursive = [&](SparseMatrix<U>& a,
                                                                              size_t expo) {
            std::cout << expo << std::endl;
            if (expo == 1) {
                return a;
            } else if ((expo & 1) != 1) {
                std::cout << "Par" << std::endl;
                auto tmp = Diamond(a, a, max_value);
                return Recursive(tmp, size_t(expo / 2));
            } else {
                std::cout << "ImPar" << std::endl;
                auto tmp = Diamond(a, a, max_value);
                auto temp2 = Recursive(tmp, size_t(expo / 2));
                return Diamond(a, temp2, max_value);
            }
        };

        Recursive(a, a.NumCols());
        return max_value;
    }

    void Dump(std::ostream& os) const {
        // Add the identification
        std::string id("SparseMatrix");
        os.write(id.data(), id.size());

        // Temp variables to store lvalue references
        size_t rows = NumRows();
        size_t cols = NumCols();

        // Save the matrix size
        os.write(reinterpret_cast<const char*>(&rows), sizeof(size_t));
        os.write(reinterpret_cast<const char*>(&cols), sizeof(size_t));

        for (auto& row : rows_) {
            row.Dump(os);
        }
    }

    void Load(std::istream& is) {
        // Retreive the identification
        std::string id("SparseMatrix");
        is.read(&id[0], id.size());

        // If the id is not valid this contructor behaves as the default
        // contructor
        if (id != "SparseMatrix") return;

        // Temp variables to store lvalue references
        size_t rows, cols;

        // Load the matrix size
        is.read(reinterpret_cast<char*>(&rows), sizeof(size_t));
        is.read(reinterpret_cast<char*>(&cols), sizeof(size_t));

        rows_.resize(rows, SparseVector<T>(cols));

        for (auto& row : rows_) {
            row.Load(is);
        }
    }

    template <typename T2>
    friend bool operator==(const SparseMatrix<T2>& m1, const SparseMatrix<T2>& m2);

private:
    std::vector<SparseVector<T>> rows_;
};

template <typename T>
std::ostream& operator<<(std::ostream& os, const SparseMatrix<T>& m) {
    os << "[";
    for (size_t j = 0; j < m.NumRows(); j++) {
        if (j != 0) os << " ";
        os << "[";
        for (size_t i = 0; i < m.NumCols(); i++) {
            os << m.Get(j, i);
            if (i != m.NumCols() - 1) os << ", ";
        }
        os << "]";
        if (j != m.NumRows() - 1) os << "\n";
    }
    os << "]";
    return os;
}

template <typename T>
bool operator==(const SparseMatrix<T>& m1, const SparseMatrix<T>& m2) {
    return (m1.rows_ == m2.rows_);
}
