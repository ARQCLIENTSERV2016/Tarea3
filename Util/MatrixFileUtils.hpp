#include <algorithm>
#include <chrono>
#include <fstream>
#include <iostream>
#include <sstream>
#include <tuple>
#include <set>

#include "Util/SparseMatrix.hpp"

template <typename T>
struct Arc {
    Arc() {}
    Arc(size_t start, size_t end, T value)
          : start(start), end(end), value(value) {}

    size_t start;
    size_t end;
    T value;
};

template <typename T>
struct ArcComparator {
    bool operator()(const Arc<T>& t1, const Arc<T>& t2) const {
        if (t1.start < t2.start) {
            return true;
        } else if (t1.start == t2.start && t1.end < t2.end) {
            return true;
        } else {
            return false;
        };
    }
};

inline bool FileExists(const std::string& name) {
    std::ifstream f(name.c_str());
    return f.good();
}

inline void CreateFile(const std::string& name) {
    std::ofstream outfile(name);
}

template <typename T>
SparseMatrix<T> LoadDatasetFromFile(const std::string& file) {
    std::ifstream dataset_file(file);

    std::string line;

    size_t num_nodes = 0;
    size_t num_arcs = 0;
    std::set<Arc<float>, ArcComparator<float>> data;

    while (std::getline(dataset_file, line)) {
        std::stringstream stream(line);

        char info;
        stream >> info;

        if (info == 'p') {
            std::string temp;
            stream >> temp;
            if (temp == "sp") {
                stream >> num_nodes >> num_arcs;
            }
        } else if (info == 'a') {
            size_t i, j;
            float value;
            stream >> i >> j >> value;
            data.emplace(i - 1, j - 1, value);
        }
    }

    if (num_arcs != data.size()) {
        std::cout << "[Warning] This file has some repeated arcs." << std::endl;
        std::cout << std::endl;
        num_arcs = data.size();
    };

    dataset_file.close();

    std::cout << "DATASET INFORMATION" << std::endl;
    std::cout << "    Number of nodes: " << num_nodes << std::endl;
    std::cout << "    Number of arcs: " << num_arcs << std::endl;

    std::cout << std::endl;

    std::cout << "Loading Matrix... " << std::flush;
    auto start = std::chrono::high_resolution_clock::now();

    SparseMatrix<float> mat(num_nodes, num_nodes);
    for (auto& arc : data) {
        mat.Set(arc.value, arc.start, arc.end);
    }

    auto end = std::chrono::high_resolution_clock::now();
    auto elapsed =
        std::chrono::duration_cast<std::chrono::duration<double>>(end - start);
    std::cout << "Done, elapsed time: " << elapsed.count() << " seconds."
              << std::endl;

    return mat;
}

template <typename T>
void DumpMatrixToFile(const SparseMatrix<T>& mat, const std::string& file) {
    std::cout << "Dumping Matrix... " << std::flush;
    auto start = std::chrono::high_resolution_clock::now();

    std::ofstream dump_file(file, std::ofstream::binary);
    mat.Dump(dump_file);
    dump_file.close();

    auto end = std::chrono::high_resolution_clock::now();
    auto elapsed =
        std::chrono::duration_cast<std::chrono::duration<double>>(end - start);
    std::cout << "Done, elapsed time: " << elapsed.count() << " seconds."
              << std::endl;
}
