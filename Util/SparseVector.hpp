#pragma once

#include <algorithm>
#include <istream>
#include <stdexcept>
#include <vector>

template <typename T>
class SparseMatrix;

template <typename T>
class SparseVector {
public:
    SparseVector() : size_(0) {}

    SparseVector(size_t size) : size_(size) {}

    SparseVector(const SparseVector<T>&) = default;

    SparseVector(SparseVector<T>&&) = default;

    SparseVector<T>& operator=(const SparseVector<T>& other) = default;

    SparseVector<T>& operator=(SparseVector<T>&& other) = default;

    size_t Size() const {
        return size_;
    }

    size_t NumNonZero() const {
        return vals_.size();
    }

    T Get(size_t pos) const {
        if (pos >= size_) {
            throw std::out_of_range("the index is not in the range");
        }

        T result(0);
        for (size_t i = 0; i < indices_.size(); i++) {
            if (indices_[i] == pos) {
                result = vals_[i];
                break;
            }
        }

        return result;
    }

    void Set(const T& val, size_t pos) {
        if (pos >= size_) {
            throw std::out_of_range("the index is not in the range");
        }

        size_t index = indices_.size();

        for (size_t i = 0; i < indices_.size(); i++) {
            if (indices_[i] == pos) {
                index = i;
                break;
            }
        }

        if (index != indices_.size()) {
            // If the value is 0 remove the index from the SparseVector
            // else change the existing value
            if (val == T(0)) {
                vals_.erase(vals_.begin() + index);
                indices_.erase(indices_.begin() + index);
            } else {
                vals_[index] = val;
            }
        } else {
            // Add a non-zero value to the SparseVector
            if (val != T(0)) {
                vals_.push_back(val);
                indices_.push_back(pos);
            }
        }
    }

    void SetAdd(const T& val, size_t pos) {
        if (pos >= size_) {
            throw std::out_of_range("the index is not in the range");
        }

        size_t index = indices_.size();

        for (size_t i = 0; i < indices_.size(); i++) {
            if (indices_[i] == pos) {
                index = i;
                break;
            }
        }

        if (index != indices_.size()) {
            vals_[index] += val;
            if (vals_[index] == T(0)) {
                vals_.erase(vals_.begin() + index);
                indices_.erase(indices_.begin() + index);
            }
        } else {
            // Add a non-zero value to the SparseVector
            if (val != T(0)) {
                vals_.push_back(val);
                indices_.push_back(pos);
            }
        }
    }
    void SetAddDiamond(const T& val, size_t pos, T& max_value) {
        if (pos >= size_) {
            throw std::out_of_range("the index is not in the range");
        }

        size_t index = indices_.size();

        for (size_t i = 0; i < indices_.size(); i++) {
            if (indices_[i] == pos) {
                index = i;
                break;
            }
        }

        if (index != indices_.size()) {
            vals_[index] = std::min(vals_[index], val);
            max_value = std::max(vals_[index], max_value);
            if (vals_[index] == T(0)) {
                vals_.erase(vals_.begin() + index);
                indices_.erase(indices_.begin() + index);
            }
        } else {
            // Add a non-zero value to the SparseVector
            if (val != T(0)) {
                vals_.push_back(val);
                indices_.push_back(pos);
            }
        }
    }

    void Dump(std::ostream& os) const {
        // Add the identificator
        std::string id("SparseVector");
        os.write(id.data(), id.size());

        // Save the vector size
        os.write(reinterpret_cast<const char*>(&size_), sizeof(size_t));

        // Temp variable to store lvalue references
        size_t temp;

        // Save the vals
        temp = vals_.size();
        os.write(reinterpret_cast<const char*>(&temp), sizeof(size_t));
        os.write(reinterpret_cast<const char*>(vals_.data()), temp * sizeof(T));

        // Save the indices
        temp = indices_.size();
        os.write(reinterpret_cast<const char*>(&temp), sizeof(size_t));
        os.write(reinterpret_cast<const char*>(indices_.data()), temp * sizeof(size_t));
    }

    void Load(std::istream& is) {
        // Retreive the identificator
        std::string id("SparseVector");
        is.read(&id[0], id.size());

        // If the id is not valid this contructor behaves as the default
        // contructor
        if (id != "SparseVector") return;

        // Load the vector size
        is.read(reinterpret_cast<char*>(&size_), sizeof(size_t));

        // Temp variable to store lvalue references
        size_t temp;

        // Load the vals
        is.read(reinterpret_cast<char*>(&temp), sizeof(size_t));
        vals_.resize(temp);
        is.read(reinterpret_cast<char*>(vals_.data()), temp * sizeof(T));

        // Load the indices
        is.read(reinterpret_cast<char*>(&temp), sizeof(size_t));
        indices_.resize(temp);
        is.read(reinterpret_cast<char*>(indices_.data()), temp * sizeof(size_t));
    }

    template <typename T2>
    friend bool operator==(const SparseVector<T2>& v1, const SparseVector<T2>& v2);

    friend SparseMatrix<T>;

private:
    size_t size_;
    std::vector<T> vals_;
    std::vector<size_t> indices_;
};

template <typename T>
std::ostream& operator<<(std::ostream& os, const SparseVector<T>& v) {
    os << "[";
    for (size_t i = 0; i < v.Size(); i++) {
        if (i) os << ' ';
        os << v.Get(i);
    }
    os << "]";
    return os;
}

template <typename T>
bool operator==(const SparseVector<T>& v1, const SparseVector<T>& v2) {
    if (v1.size_ != v2.size_) {
        return false;
    }

    if (v1.indices_.size() != v2.indices_.size()) {
        return false;
    }

    for (auto& index : v1.indices_) {
        if (v1.Get(index) != v2.Get(index)) {
            return false;
        }
    }

    return true;
}
