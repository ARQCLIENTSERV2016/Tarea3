#pragma once

#include <sstream>
#include <stdexcept>
#include <vector>

#include "Matrix.hpp"

///////////////////////////////////////////////////////////////////////////////
// Reference Matrix
// Used for submatrix Matrix access
///////////////////////////////////////////////////////////////////////////////

template <typename T>
class Matrix<T*> {
public:
    Matrix(size_t rows, size_t cols)
          : num_rows_(rows), num_cols_(cols), data_(rows * cols, nullptr) {}

    template <typename U>
    Matrix(Matrix<U>& m, size_t row_pos, size_t col_pos, size_t num_rows,
           size_t num_cols)
          : num_rows_(num_rows),
            num_cols_(num_cols),
            data_(num_rows * num_cols, nullptr) {
        size_t end_row = (row_pos + NumRows());
        size_t end_col = (col_pos + NumCols());

        if (end_row > m.NumRows() || end_col > m.NumCols()) {
            throw std::runtime_error("out of matrix bounds.");
        }

        size_t cont = 0;
        for (size_t row = row_pos; row < end_row; row++) {
            for (size_t col = col_pos; col < end_col; col++) {
                data_[cont++] = &m(row, col);
            }
        }
    }

    Matrix<T*>& operator=(const Matrix<T>& m2) {
        if (NumRows() != m2.NumRows() || NumCols() != m2.NumCols()) {
            std::stringstream stream;
            stream << "can't assign matrices of dimesions [" << NumRows()
                   << ", " << NumCols() << "] and [" << m2.NumRows() << ", "
                   << m2.NumCols() << "]\n";
            throw std::runtime_error(stream.str());
        }

        for (size_t row = 0; row < NumRows(); row++) {
            for (size_t col = 0; col < NumCols(); col++) {
                (*this)(row, col) = m2(row, col);
            }
        }

        return *this;
    }

public:
    bool SetData(const std::vector<T>& other) {
        if (other.size() <= data_.size()) {
            for (int i = 0; i < other.size(); ++i) {
                *data_[i] = other[i];
            }
            return true;
        }
        return false;
    }

    T& operator()(size_t row, size_t col) {
        return *data_[num_cols_ * row + col];
    }

    const T& operator()(size_t row, size_t col) const {
        return *data_[num_cols_ * row + col];
    }

    size_t NumRows() const {
        return num_rows_;
    }

    size_t NumCols() const {
        return num_cols_;
    }

private:
    size_t num_rows_;
    size_t num_cols_;
    std::vector<T*> data_;
};

template <typename T>
Matrix<T> operator+(const Matrix<T*>& m1, const Matrix<T*>& m2) {
    return operator+<T*, T>(m1, m2);
}

template <typename T>
Matrix<T> operator*(const Matrix<T*>& m1, const Matrix<T*>& m2) {
    return operator*<T*, T>(m1, m2);
}
