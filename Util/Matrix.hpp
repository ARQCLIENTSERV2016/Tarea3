#pragma once

#include <ostream>
#include <sstream>
#include <stdexcept>
#include <vector>

#include "RVector.hpp"

template <typename T>
class Matrix {
public:
    Matrix() : num_rows_(0), num_cols_(0), data_(0) {}

    Matrix(const Matrix<T>& other) = default;  // Copy constructor

    Matrix(Matrix<T>&& other) = default;  // Move constructor

    Matrix(size_t rows, size_t cols)
          : num_rows_(rows), num_cols_(cols), data_(rows * cols, T(0)) {}

    template <typename U>
    Matrix(const Matrix<U>& m, size_t row_pos, size_t col_pos, size_t num_rows,
           size_t num_cols)
          : num_rows_(num_rows),
            num_cols_(num_cols),
            data_(num_rows * num_cols, T(0)) {
        size_t end_row = (row_pos + NumRows());
        size_t end_col = (col_pos + NumCols());

        if (end_row > m.NumRows() || end_col > m.NumCols()) {
            throw std::runtime_error("out of matrix bounds.");
        }

        size_t cont = 0;
        for (size_t row = row_pos; row < end_row; row++) {
            for (size_t col = col_pos; col < end_col; col++) {
                data_[cont++] = m(row, col);
            }
        }
    }

    Matrix<T>& operator=(const Matrix<T>& other) = default;

    Matrix<T>& operator=(Matrix<T>&& other) = default;

    void SetSize(size_t rows, size_t cols) {
        num_rows_ = rows;
        num_cols_ = cols;
        data_.resize(num_rows_ * num_cols_);
    }

    bool SetData(std::vector<T>&& other) {
        if (other.size() == data_.size()) {
            data_ = std::move(other);
            return true;
        }
        return false;
    }

    bool SetData(const std::vector<T>& other) {
        if (other.size() == data_.size()) {
            data_ = other;
            return true;
        }
        return false;
    }

    std::vector<T>& GetData() {
        return data_;
    }

    const std::vector<T>& GetData() const {
        return data_;
    }

    RVector<T> GetRow(size_t pos) {
        if (pos >= NumRows()) {
            // If out of bounds return an empty RVector
            return RVector<T>();
        }

        std::vector<T*> data(NumCols(), nullptr);

        for (size_t i = 0; i < NumCols(); i++) {
            data[i] = &(*this)(i, pos);
        }

        return RVector<T>(std::move(data));
    }

    RVector<T> GetCol(size_t pos) {
        if (pos >= NumCols()) {
            // If out of bounds return an empty RVector
            return RVector<T>();
        }

        std::vector<T*> data(NumRows(), nullptr);

        for (size_t j = 0; j < NumRows(); j++) {
            data[j] = &(*this)(pos, j);
        }

        return RVector<T>(std::move(data));
    }

    T& operator()(size_t row, size_t col) {
        return data_[num_cols_ * row + col];
    }

    const T& operator()(size_t row, size_t col) const {
        return data_[num_cols_ * row + col];
    }

    size_t NumRows() const {
        return num_rows_;
    }

    size_t NumCols() const {
        return num_cols_;
    }

private:
    size_t num_rows_;
    size_t num_cols_;
    std::vector<T> data_;
};

template <typename T>
std::ostream& operator<<(std::ostream& os, const Matrix<T>& m) {
    os << "[";
    for (size_t j = 0; j < m.NumRows(); j++) {
        if (j != 0) os << " ";
        os << "[";
        for (size_t i = 0; i < m.NumCols(); i++) {
            os << m(j, i);
            if (i != m.NumCols() - 1) os << ", ";
        }
        os << "]";
        if (j != m.NumRows() - 1) os << "\n";
    }
    os << "]";
    return os;
}

template <typename T, typename U = T>
Matrix<U> operator+(const Matrix<T>& m1, const Matrix<T>& m2) {
    if (m1.NumRows() != m2.NumRows() || m1.NumCols() != m2.NumCols()) {
        std::stringstream stream;
        stream << "can't add matrices of dimesions [" << m1.NumRows() << ", "
               << m1.NumCols() << "] and [" << m2.NumRows() << ", "
               << m2.NumCols() << "]\n";
        throw std::runtime_error(stream.str());
    }

    Matrix<U> result(m1.NumRows(), m1.NumCols());

    for (size_t row = 0; row < m1.NumRows(); row++) {
        for (size_t col = 0; col < m1.NumCols(); col++) {
            result(row, col) += m1(row, col) + m2(row, col);
        }
    }

    return result;
}

template <typename T, typename U = T>
Matrix<U> operator*(const Matrix<T>& m1, const Matrix<T>& m2) {
    if (m1.NumCols() != m2.NumRows()) {
        std::stringstream stream;
        stream << "can't multiply matrices of dimensions [" << m1.NumRows()
               << ", " << m1.NumCols() << "] and [" << m2.NumRows() << ", "
               << m2.NumCols() << "]\n";
        throw std::runtime_error(stream.str());
    }

    Matrix<U> result(m1.NumRows(), m2.NumCols());

    for (size_t row = 0; row < m1.NumRows(); row++) {
        for (size_t col = 0; col < m2.NumCols(); col++) {
            for (size_t num = 0; num < m1.NumCols(); num++) {
                result(row, col) += m1(row, num) * m2(num, col);
            }
        }
    }

    return result;
}

template <typename T>
std::ostream& operator<<(std::ostream& os, const RVector<T>& v) {
    os << "[";
    for (size_t i = 0; i < v.Size(); i++) {
        os << v[i];
        if (i != v.Size() - 1) os << ", ";
    }
    os << "]";
    return os;
}

template <typename T>
bool operator==(const Matrix<T>& m1, const Matrix<T>& m2) {
    return (m1.NumCols() == m2.NumCols() && m1.NumRows() == m2.NumRows() &&
            m1.GetData() == m2.GetData());
}

template <typename T>
bool operator!=(const Matrix<T>& m1, const Matrix<T>& m2) {
    return !(m1 == m2);
}
