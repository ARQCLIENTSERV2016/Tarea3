#include <chrono>
#include <iostream>
#include <thread>

#include "Util/MatrixFileUtils.hpp"
#include "Util/SparseMatrix.hpp"

int main(int argc, char* argv[]) {
    if (argc != 2) {
        std::cout << "Usage: " << argv[0] << " dataset" << std::endl;
        return 1;
    }

    if (!FileExists(argv[1])) {
        std::cout << "File " << argv[1] << " does not exists." << std::endl;
        return 2;
    }

    SparseMatrix<float> mat = LoadDatasetFromFile<float>(argv[1]);

    /*SparseMatrix<float> result1;
    {
        std::cout << "Multiplying Sequencial Old... " << std::flush;
        auto start = std::chrono::high_resolution_clock::now();
        result1 = mat.MultOld(mat);
        auto end = std::chrono::high_resolution_clock::now();
        auto elapsed =
            std::chrono::duration_cast<std::chrono::duration<double>>(end -
                                                                      start);
        std::cout << "Done, elapsed time: " << elapsed.count() << " seconds."
                  << std::endl;
    }*/
    // {
    //     std::cout << "Diamond Recursive Concurrent... " << std::flush;
    //     auto start = std::chrono::high_resolution_clock::now();
    //     mat.DiamondRecursive(mat);
    //     // float val = std::numeric_limits<float>::min();
    //     // SparseMatrix<float>::Diamond(mat, mat, val);
    //     auto end = std::chrono::high_resolution_clock::now();
    //     auto elapsed = std::chrono::duration_cast<std::chrono::duration<double>>(end - start);
    //     std::cout << "Done, elapsed time: " << elapsed.count() << " seconds." << std::endl;
    //     std::this_thread::sleep_for(std::chrono::seconds(10));
    // }

    SparseMatrix<float> result2;
    {
        std::cout << "Multiplying Sequencial... " << std::flush;
        auto start = std::chrono::high_resolution_clock::now();
        result2 =
            mat.Mult(mat)
                .Mult(mat.Mult(mat))
                .Mult(mat.Mult(mat).Mult(mat.Mult(mat)))
                .Mult(mat.Mult(mat).Mult(mat.Mult(mat)).Mult(mat.Mult(mat).Mult(mat.Mult(mat))));
        auto end = std::chrono::high_resolution_clock::now();
        auto elapsed = std::chrono::duration_cast<std::chrono::duration<double>>(end - start);
        std::cout << "Done, elapsed time: " << elapsed.count() << " seconds." << std::endl;
    }

    std::cout << result2 << std::endl;
    // std::this_thread::sleep_for(std::chrono::seconds(10));
    /*std::cout << std::boolalpha;
    std::cout << "Are equal?: " << (result1 == result2) << std::endl;
*/
    return 0;
}
