#include <chrono>
#include <algorithm>
#include <iostream>
#include <random>

#include "Util/SparseMatrix.hpp"

template <typename T>
std::vector<T> RandomData(size_t rows, size_t cols, int min = 1, int max = 9,
                          float zeros = 0.5f) {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<int> dis(min, max);

    size_t data_size = rows * cols;

    std::vector<T> data((data_size * zeros), T(0));
    data.reserve(data_size);

    for (size_t i = 0; i < data_size - (data_size * zeros); i++) {
        data.push_back(static_cast<T>(dis(gen)));
    }

    std::random_shuffle(data.begin(), data.end());

    return data;
}

int main(int, char* []) {
    int m1_rows = 500;
    int m1_cols = 500;
    int m2_rows = 500;
    int m2_cols = 500;
    float zeros = 0;
    bool print = false;

    auto data = RandomData<float>(m1_rows, m1_cols, 1, 9, zeros);
    auto data2 = RandomData<float>(m2_rows, m2_cols, 1, 9, zeros);

    SparseMatrix<float> sm(m1_rows, m1_cols);
    SparseMatrix<float> sm2(m2_rows, m2_cols);
    sm.SetData(data);
    sm2.SetData(data2);

    std::cout << "Multiplying [" << m1_rows << ", " << m1_cols << "] * ["
              << m2_rows << ", " << m2_cols << "]" << std::endl;

    SparseMatrix<float> r1, r2;

    {
        auto start = std::chrono::high_resolution_clock::now();
        r1 = sm.Mult(sm2);
        auto end = std::chrono::high_resolution_clock::now();
        auto elapsed =
            std::chrono::duration_cast<std::chrono::duration<double>>(end -
                                                                      start);
        if (print) std::cout << r1 << std::endl;
        std::cout << "Sequential: " << elapsed.count() << "s" << std::endl;
    }

    {
        auto start = std::chrono::high_resolution_clock::now();
        r2 = sm.MultConcurrent(sm2);
        auto end = std::chrono::high_resolution_clock::now();
        auto elapsed =
            std::chrono::duration_cast<std::chrono::duration<double>>(end -
                                                                      start);
        if (print) std::cout << r2 << std::endl;
        std::cout << "Concurrent: " << elapsed.count() << "s" << std::endl;
    }

    std::cout << "Are equal: " << std::boolalpha << (r1 == r2) << std::endl;

    return 0;
}
