#include <algorithm>
#include <chrono>
#include <iostream>
#include <random>
#include <thread>

#include "Util/Matrix.hpp"

#define TAM 1024

using namespace std;
using namespace std::chrono;

bool SetSubMatrix(Matrix<float>& m1, const Matrix<float> m2, size_t row_pos,
                  size_t col_pos) {
    size_t end_row = (row_pos + m2.NumRows());
    size_t end_col = (col_pos + m2.NumCols());

    if (end_row > m1.NumRows() || end_col > m1.NumCols()) {
        throw std::runtime_error("out of matrix bounds.");
    }

    for (size_t row = row_pos; row < end_row; row++) {
        for (size_t col = col_pos; col < end_col; col++) {
            m1(row, col) = m2(row - row_pos, col - col_pos);
        }
    }

    return true;
}

Matrix<float> MultBlock(const Matrix<float>& a, const Matrix<float>& b) {
    if (a.NumRows() == 32) {
        return a * b;
    } else {
        // Obtengo las mitades de cada matriz
        size_t a_nrows = a.NumRows() / 2;
        size_t a_ncols = a.NumCols() / 2;
        size_t b_nrows = b.NumRows() / 2;
        size_t b_ncols = b.NumCols() / 2;

        // Obtengo cada sub matriz de la matriz a
        Matrix<float> a00(a, 0, 0, a_nrows, a_ncols);
        Matrix<float> a10(a, a_nrows, 0, a_nrows, a_ncols);
        Matrix<float> a01(a, 0, a_ncols, a_nrows, a_ncols);
        Matrix<float> a11(a, a_nrows, a_ncols, a_nrows, a_ncols);

        // Obtengo cada sub matriz de la matriz b
        Matrix<float> b00(b, 0, 0, b_nrows, b_ncols);
        Matrix<float> b10(b, b_nrows, 0, b_nrows, b_ncols);
        Matrix<float> b01(b, 0, b_ncols, b_nrows, b_ncols);
        Matrix<float> b11(b, b_nrows, b_ncols, b_nrows, b_ncols);

        // Hago las multiplicaciones de cada cuadrante de la matriz
        Matrix<float> r00 = MultBlock(a00, b00) + MultBlock(a01, b10);
        Matrix<float> r10 = MultBlock(a10, b00) + MultBlock(a11, b10);
        Matrix<float> r01 = MultBlock(a00, b01) + MultBlock(a01, b11);
        Matrix<float> r11 = MultBlock(a10, b01) + MultBlock(a11, b11);

        // Creo la matriz resultante
        Matrix<float> result(a.NumRows(), b.NumCols());

        // Guardo cada solución de un cuadrante en la matriz resultante
        SetSubMatrix(result, r00, 0, 0);
        SetSubMatrix(result, r10, a_nrows, 0);
        SetSubMatrix(result, r01, 0, b_ncols);
        SetSubMatrix(result, r11, a_nrows, b_ncols);

        return result;
    }
}

Matrix<float> Multyply(const Matrix<float>& m1, const Matrix<float>& m2) {
    return MultBlock(m1, m2);
}

Matrix<float> RandomMatrix(size_t rows, size_t cols, int min = 0, int max = 9) {
    random_device rd;
    mt19937 gen(rd());
    uniform_int_distribution<int> dis(min, max);

    Matrix<float> m(rows, cols);

    generate(m.GetData().begin(), m.GetData().end(),
             [&] { return static_cast<float>(dis(gen)); });

    return m;
}

int main() {
    Matrix<float> m1 = RandomMatrix(TAM, TAM);
    Matrix<float> m2 = RandomMatrix(TAM, TAM);

    cout << "Multiplicando matrices de tamaño [" << m1.NumRows() << ", "
         << m1.NumCols() << "] * [" << m2.NumRows() << ", " << m2.NumCols()
         << "]\n";

    Matrix<float> r1, r2;

    {
        auto tstart = high_resolution_clock::now();
        r1 = Multyply(m1, m2);
        auto tend = high_resolution_clock::now();
        auto elapsed = duration_cast<nanoseconds>(tend - tstart).count();
        cout << "Bloques: " << elapsed / 1000000000.0 << " s" << endl;
    }

    {
        auto tstart = high_resolution_clock::now();
        r2 = (m1 * m2);
        auto tend = high_resolution_clock::now();
        auto elapsed = duration_cast<nanoseconds>(tend - tstart).count();
        cout << "Normal: " << elapsed / 1000000000.0 << " s" << endl;
    }

    cout << "Las matrices son iguales: " << boolalpha << (r1 == r2) << endl;
    return 0;
}
